<?php


class Building{
     public $name;
     public $floors;
     public $address;

     public function __construct($name, $floors, $address){
          $this->name = $name;
          $this->floors = $floors;
          $this->address = $address;
     }

     public function getName(){
          return $this->name;

     }
     
     public function setName($name){
          $this->name = $name;
          
     }

     private function getFloors(){
          return $this->floors;

     }
     
     private function setFloors($floors){
               $this->floors = $floors;
          
     }

     private function getAddress(){
          return $this->address;

     }
     
     private function setAddress($address){
               $this->address = $address;
          
     }


}

$building = new Building("Caswynn Bldg", 8, "Timog Ave. Quezon City, Philippines");


class Condominium extends Building{
     
}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');