<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
     <head>
          <meta charset="UTF-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Document</title>
     </head>
     <body>
          <h1>Building</h1>
          <p>The name of the building is <?= $building->name?></p>
          <p>The <?= $building->name ?> has <?= $building->floors ?> floors</p>
          <p>The <?= $building->name ?> is located at <?= $building->address ?>.</p>
          <?php $building->setName("Caswynn Complex") ?>
          <p> The name of the building has been changed to <?= $building->getName() ?></p>
          <br>

          <h1>Condominium</h1>
          <p>The name of the condominium is <?= $condominium->name?></p>
          <p>The <?= $condominium->name ?> has <?= $condominium->floors ?> floors</p>
          <p>The <?= $condominium->name ?> is located at <?= $condominium->address ?>.</p>
          <?php $condominium->setName("Enzo Tower") ?>
          <p> The name of the condominium has been changed to <?= $condominium->getName() ?></p>
     </body>
</html>